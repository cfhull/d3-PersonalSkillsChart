$(document).ready(function(){
	
	// set up svg margins and dimensions
	var margin = {top: 20, right: 30, bottom: 40, left: 80},
    svgWidth   = 960 - margin.left - margin.right,
    svgHeight  = 500 - margin.top - margin.bottom;
	
	// set up toolTip
	var toolTipWidth  = 200;
	var toolTipHeight = 200;
	
	// set padding for chart
	var xPadding   = 100;
	var yPadding   = 20;
	
	var barPadding = 20;
	
	// create and get svg element
	var svg = d3.select("body").append("svg")
						   .attr("width", svgWidth + margin.left + margin.right)
						   .attr("height", svgHeight + margin.top + margin.bottom)
						   .append("g")
						   .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
				
	// set up svg defs for gradients
	svg.append("svg:defs");
	
	// set gradients colors [startColor, endColor]
	var gradientColors = [["#6F0F0A", "#C45953"],  //red
						  ["#0F204D", "#405385"],  //blue
						  ["#085612", "#40984B"],  //green
						  ["#5B0834", "#A04474"],  //magenta
						  ["#073E44", "#337177"],  //indigo
						  ["#6F4E0A", "#C49F52"]]; //gold							  						  
						 	
	// for each gradient color, create a gradient svg element. 
	$.each(gradientColors, function(i, gradientColor){
		
		var gradient = svg.select("defs")
						  .append("svg:linearGradient")
						  .attr("id", "gradient" + i)
						  .attr("x1", "0%")
						  .attr("y1", "50%")
						  .attr("x2", "100%")
						  .attr("y2", "50%")
						  .attr("spreadMethod", "pad");

		gradient.append("svg:stop")
				.attr("offset", "0%")
				.attr("stop-color", gradientColors[i][0])
				.attr("stop-opacity", 1);
				
		gradient.append("svg:stop")
				.attr("offset", "100%")
				.attr("stop-color", gradientColors[i][1])
				.attr("stop-opacity", 1);
	});

	
		// import csv data
		d3.csv("data/data.csv", type, function(data) {
		
		// set scales
		var xScale = d3.scale.linear()
					   .domain([0, 5])
					   .range([0, svgWidth]);		
		
		var yScale = d3.scale.ordinal()
					   .domain(data.map(function(d){ return d.name; }))
					   .rangeBands([0, svgHeight]);
					   
		// set axes
		var xAxis = d3.svg.axis()
					  .scale(xScale)
					  .orient("bottom")
					  .ticks(5);		
		
		// create bar elements
		var bars = svg.selectAll("rect")
					  .data(data)
					  .enter()
					  .append("rect")
						  .style("fill", function(d, i){ return "url(#gradient" + (i % gradientColors.length) + ")"; })
						  .attr("class", "bar")
						  .attr("height", svgHeight / data.length - barPadding)
						  .attr("width", function(d){ return 0; })
						  .attr("y", function(d){ return yScale(d.name); });
							
		bars.on("click", function(){
			selectBar(d3.select(this), bars);
			toggleDescription(d3.select(this));
		});
		
		bars.transition()
			.delay(function(d, i) { return i * 100; })
			.duration(1000)
			.attr("width", function(d){ return xScale(d.value); });
		
		// create axes elements
		svg.append("g")
		   .attr("class", "x axis")
		   .call(xAxis)
		   .attr("transform", "translate(0," + (svgHeight - barPadding / 2) + ")")
		   
		// set labels
		var yLabels = svg.append("g").attr("class", "yLabel");
		
		yLabels.selectAll("text")
					   .data(data)
					   .enter()
					   .append("text")
					   .text(function(d) { return d.name; })
					   .attr("x", 5)	
					   .attr("y", function(d, i){ return i * (svgHeight / data.length) + (svgHeight / data.length - barPadding) / 2; });		
				
	});
	
	// highlights selection
	function selectBar(selectedBar, bars){
		
		// set all bars back to default styling
		bars.style("stroke", "none")
			.style("fill-opacity", "1.0");
		
		// set selected bar to "highlighted" styling
		selectedBar.style("stroke", "black")
				   .style("fill-opacity", "0.5")	
	}
	
	function toggleDescription(selectedBar){
	var check = selectedBar.datum().name;
		if ( check == "Git"){
			alert("IT WORKED!!!");
			
		}
	}

	function type(d){
	  d.value = +d.value;
	  return d;
	}
});